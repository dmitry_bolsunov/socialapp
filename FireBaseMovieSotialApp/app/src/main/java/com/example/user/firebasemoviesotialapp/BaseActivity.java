package com.example.user.firebasemoviesotialapp;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.user.firebasemoviesotialapp.databinding.ActivityBaseBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class BaseActivity extends AppCompatActivity {


    private ActivityBaseBinding mBinding;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_base);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_base);
        mBinding.setActivity(this);
        mBinding.setErrorText("");
        mAuth = FirebaseAuth.getInstance();
    }


    public void SignIn() {
        mBinding.setErrorText("");
        String email = mBinding.login.getText().toString();
        String password = mBinding.password.getText().toString();
        if(email.equals("") || password.equals("")) return;
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                        }
                        else {

                            mBinding.setErrorText(task.getException().getMessage());
                        }
                    }
                });

    }


    public void SignUp(){
        mBinding.setErrorText("");
        String email = mBinding.login.getText().toString();
        String password = mBinding.password.getText().toString();
        if(email.equals("") || password.equals("")) return;
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("MYDEBUG", "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "FAILED TO CREATE ACCOUNT",
                                    Toast.LENGTH_LONG).show();
                            mBinding.setErrorText(task.getException().getMessage());
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "SUCSESS",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
